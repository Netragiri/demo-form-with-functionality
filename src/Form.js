import React, { useState } from 'react'

function Form() {
    const[text,setText]=useState("")

    const handlechange=(event)=>{
        setText(event.target.value)
    }
    const upperhandler=()=>{
        setText(text.toUpperCase())
    }
    const lowerhandler=()=>{
        setText(text.toLowerCase())
    }
    const clearhandler=()=>{
        let string=text.toLocaleString()
        setText(string="")
    }
  return (
    <div className='container my-4' style={{'textAlign':'center'}}>
        <h2>Enter text to analyze</h2>
        <label htmlFor='txt'></label>
        <textarea placeholder='enter text' value={text} id='txt' rows='10' cols='80' onChange={handlechange} style={{'fontSize':'20px'}}></textarea><br></br>
        
        <div style={{'textAlign':'left'}}>
            <button className='mx-2 my-3' onClick={upperhandler}>To upper case</button>
            <button className='mx-2 my-3' onClick={lowerhandler}>To lower case</button>
            <button className='mx-2 my-3' onClick={clearhandler}>Clear text</button>

            <p><b>Total letter:  </b> {text.length} <b>  Total words:</b> {text.split(" ").length}</p>
            <h3>Preview text</h3>
            <p>{text}</p>
        </div>     
       
     </div> 
     
  )
}

export default Form
